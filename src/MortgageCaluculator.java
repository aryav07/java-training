import java.util.Scanner;

public class MortgageCaluculator {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter principal amount");
		int principal = sc.nextInt();
		System.out.println("Enter anual interest");
		int annualInterest = sc.nextInt();
		float monthlyInterest = annualInterest/1200;
		System.out.println("Enter loan period");
		int period = sc.nextInt();
		int noOfPayments = period*12;
		double mortgage;
		mortgage = principal*
				(monthlyInterest*
						Math.pow(1+ monthlyInterest,noOfPayments ))
				/(Math.pow(1+ monthlyInterest, noOfPayments)-1);
		System.out.println(mortgage);
}
}