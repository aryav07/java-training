import java.util.Scanner;

public class TakingInput {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter name");
		String name = sc.nextLine();
		System.out.println("Enter city");
		String city = sc.next();
		System.out.println("Enter Age");
		int age = sc.nextInt();
		System.out.println("Temperature");
		float Temperature = sc.nextFloat();
		
		System.out.println("Hey "+name+" ' you are "+age+" years old");
		System.out.println("Currently you are in "+city);
		System.out.println("Temperature there is "+Temperature+" degree celcius");
	}
}
