
public class SwitchCase {
	public static void main(String[] args) {
		String role ="Admin";
		switch(role) {
		case "Developer":
			System.out.println("Password is Dev@123");
			break;
		case "Admin":
			System.out.println("Password is Admin@123");
			break;
		case "Tester":
			System.out.println("Password is Tester@123");
			break;
		default:
			System.out.println("Password is Guest@123");	
		}
	}
}
