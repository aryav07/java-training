
public class LoopsWhile {
	public static void main(String[] args) {
		int i=1;
		while(i<=10) {
			System.out.println(i);
			i++;
		}
		System.out.println("---------------------------");
		int j=20;
		do {
			System.out.println(j);
		}while(j<=15);
		System.out.println("---------------------------");
		for(int k =1;k<=10;k++) {
			System.out.println(k);
		}
	}
}
